from django.shortcuts import render
from .models import TodoList


# Create your views here.
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "list_object": list,
    }
    return render(request, "todos/list.html", context)
